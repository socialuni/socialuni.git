import CenterUserDetailRO from './social/CenterUserDetailRO'
import SocialuniChatRO from 'socialuni-api-base/src/model/SocialuniChatRO'

export default class NotifyVO {
    // public user: CenterUserDetailRO
    // public chat: SocialuniChatRO
    // public domainType: string
    // public contentType: string
    // public hintMsg: string
    // public createTime: Date
    public type: string
    public data: any
}
