export default class SocialuniAppConst {
    private static readonly socialuni = 'socialuni_'
    static readonly deviceUidKey = SocialuniAppConst.socialuni + "deviceUid"
    static readonly logoutStatusKey = SocialuniAppConst.socialuni + "logout"
}
