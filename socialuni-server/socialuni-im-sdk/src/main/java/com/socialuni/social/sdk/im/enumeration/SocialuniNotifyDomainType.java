package com.socialuni.social.sdk.im.enumeration;

import java.util.Arrays;
import java.util.List;

/**
 * 关系类型枚举
 *
 * @author qinkaiyuan
 * @date 2018-09-16 10:58
 */
public class SocialuniNotifyDomainType {
    //只有三种，一种通知，一种操作
    public static final String cp = "cp";
    public static final String comment = "comment";
}

