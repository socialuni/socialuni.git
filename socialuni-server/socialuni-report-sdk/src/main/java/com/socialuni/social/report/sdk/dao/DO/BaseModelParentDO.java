package com.socialuni.social.report.sdk.dao.DO;


public interface BaseModelParentDO {

    void setTalkId(Long talkId);

    void setMessageId(Long messageId);

    void setCommentId(Long commentId);

    void setUserImgId(Long userImgId);
}
